﻿using FaceWebAuth.FacebookDataSynchronizer;
using FaceWebAuth.Models;
using FaceWebAuth.TensorFlow;
using FaceWebAuth.TensorFlow.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using VDS.RDF;
using VDS.RDF.Query;
using Microsoft.Extensions.Logging;

namespace FaceWebAuth.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<HomeController> _logger;

        public HomeController(IConfiguration configuration, ILogger<HomeController> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public async Task<IActionResult> RunClassification()
        {
            return View(await new FusekiSynchronizer(_configuration).ClassifyFacebookImages());
        }

        [Authorize]
        public async Task<IActionResult> ClassifyImage(string url)
        {
            var tensor = new TensorMachineLearning(_configuration);
            return View(string.IsNullOrEmpty(url) ? null :
                new Tuple<string, IEnumerable<ClassifyImageResult>> (url, await tensor.ClassifyImageByUri(url)));
        }

        [Authorize]
        public async Task<IActionResult> TestToken(string token)
        {
            Tuple<string, IEnumerable<Triple>> result;
            if (!string.IsNullOrEmpty(token))
            {
                using (var fbCollector = new FacebookCollector(_configuration, token))
                {
                    result = new Tuple<string, IEnumerable<Triple>>(token, (await fbCollector.MeDynamicComplex())?.Triples);
                }
            }
            else
            {
                result = null;
            }
            return View(result);    
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddFbTriplesByToken([FromBody]string token)
        {
            _logger.LogInformation("Initialized AddFbTriplesByToken");
            if (!string.IsNullOrEmpty(token))
            {
                using (var fusekiSync = new FusekiSynchronizer(_configuration, token))
                {
                    await fusekiSync.SyncFacebookMeDataDynamic();
                }
            }
            else
            {
                return new StatusCodeResult(422);
            }
            return Ok();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult PrivacyPolicy()
        {
            return View();
        }

        public IActionResult ServicesTerms()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> SPARQL(string query)
        {
            SparqlResultSet result;
            using (var connector = new FusekiApiConnector(_configuration))
            {
                result = await connector.PerformSelectQuery(query);
            }
            return View(new Tuple<string, SparqlResultSet>(query, result));
        }

        [HttpGet]
        [Authorize]
        public IActionResult SPARQL()
        {
            return View();
        }

        [Authorize]
        public async Task<IActionResult> Graph(string url)
        {
            if(string.IsNullOrEmpty(url))
            {
                url = "https://www.facebook.com/fuseki/";
            }
            IGraph g;
            using (var connector = new FusekiApiConnector(_configuration))
            {
                g = await connector.LoadGraph(url);
            }
            return View(new Tuple<string, IEnumerable<Triple>>(url, g?.Triples));
        }

        public async Task<IActionResult> DeleteGraph()
        {
            using (var connector = new FusekiApiConnector(_configuration))
            {
                await connector.DeleteGraph("https://www.facebook.com/fuseki/");
            }
            return Content("Deleted graph: https://www.facebook.com/fuseki/");
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
