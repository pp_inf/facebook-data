﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FaceWebAuth.FacebookDataSynchronizer.GraphApi;
using FaceWebAuth.FacebookDataSynchronizer.GraphApi.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using VDS.RDF;

namespace FaceWebAuth.FacebookDataSynchronizer
{
    /// <summary>
    /// Class for Collecting facebook data
    /// </summary>
    public class FacebookCollector : GraphApiConnector
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="configuration">Configuration</param>
        /// <param name="accessToken">Facebook Graph Api Access token</param>
        public FacebookCollector(IConfiguration configuration, string accessToken) : base(configuration, accessToken)
        {
        }

        /// <summary>
        /// Get permissed action for access token
        /// </summary>
        /// <returns>Collection with Api Permissions</returns>
        private async Task<IEnumerable<ApiPermission>> MePermissions()
        {
            return (await GetMePermissions())["data"].ToObject<IEnumerable<ApiPermission>>();
        }

        /// <summary>
        /// Get permissed action for access token
        /// </summary>
        /// <returns>Collection with Api Permissions</returns>
        private async Task<JObject> GetMePermissions()
        {
            return await Get(Node.me, Edges.permissions);
        }

        /// <summary>
        /// Get dynamically complex information about facebook user
        /// </summary>
        /// <returns>Graph with facebook triples</returns>
        public async Task<IGraph> MeDynamicComplex()
        {
            var fbComplex = new FacebookGraph();
            var data = await GetComplexData();
            await fbComplex.AddFacebookObjectAsync(data["id"].Value<string>(), FacebookNodeType.user, data);
            return fbComplex.BaseGraph;
        }

    }
}
