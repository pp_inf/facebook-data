﻿using FaceWebAuth.Extensions;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using VDS.RDF;
using VDS.RDF.Query;
using VDS.RDF.Storage;
using NLog;

namespace FaceWebAuth.FacebookDataSynchronizer
{
    /// <summary>
    /// Connector to fuseki RDF Storaga
    /// </summary>
    public class FusekiApiConnector : IDisposable
    {
        /// <summary>
        /// Logger object
        /// </summary>
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// Configuration
        /// </summary>
        protected IConfiguration _configuration;
        /// <summary>
        /// Disposed variable
        /// </summary>
        protected bool _isDisposed = false;
        /// <summary>
        /// Connector to fuseki
        /// </summary>
        private readonly FusekiConnector _fusekiConnector;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="configuration"></param>
        public FusekiApiConnector(IConfiguration configuration)
        {
            _fusekiConnector = new FusekiConnector(configuration.GetFusekiServiceUri());
            _configuration = configuration;
        }

        /// <summary>
        /// Perform SPARQL select query
        /// </summary>
        /// <param name="query">Select query</param>
        /// <returns>Reuslt of Select query</returns>
        public async Task<SparqlResultSet> PerformSelectQuery(string query)
        {
            var task = Task.Run(() => { return (SparqlResultSet)_fusekiConnector.Query(query); });
            _logger.Info($"Performed sparql: {query}");
            return await task;
        }

        /// <summary>
        /// Load Graph
        /// </summary>
        /// <param name="graphUri">Graph uri</param>
        /// <returns>Collection of triples in graph object</returns>
        public async Task<Graph> LoadGraph(string graphUri)
        {
            return await Task.Run(() =>
            {
                Graph g = new Graph();
                _fusekiConnector.LoadGraph(g, graphUri);
                _logger.Info($"Loaded graph <{graphUri}>");
                return g;
            });
        }

        /// <summary>
        /// Delete graph
        /// </summary>
        /// <param name="graphUri">Graph uri</param>
        /// <returns>Task</returns>
        public async Task DeleteGraph(string graphUri)
        {
            await Task.Run(() => _fusekiConnector.DeleteGraph(graphUri));
            _logger.Info($"Deleted graph <{graphUri}>");
        }

        /// <summary>
        /// Perform update graph
        /// </summary>
        /// <param name="graph">Graph which will be updated</param>
        /// <returns>Task</returns>
        public async Task PerformUpdateGraph(IGraph graph)
        {
            await Task.Run(() =>
            {
                if (!_fusekiConnector.UpdateSupported)
                {
                    _logger.Warn($"Update unsupported");
                    return;
                }
                _logger.Info($"Graph <{graph.BaseUri}> updated");
                _fusekiConnector.UpdateGraph(graph.BaseUri, graph.Triples, null);
            });
        }

        /// <summary>
        /// Dispose implementation
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose imlementation
        /// </summary>
        /// <param name="disposing"></param>
        virtual protected void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    // tu zwalniamy zasoby zarządzane (standardowe klasy)
                }
                // tu zwalniamy zasoby niezarządzane (np. strumienie, obiekty COM itp.)
                _fusekiConnector?.Dispose();
            }
            _isDisposed = true;
        }

        /// <summary>
        /// Destructor
        /// </summary>
        ~FusekiApiConnector()
        {
           Dispose(false);
        }
    }
}
