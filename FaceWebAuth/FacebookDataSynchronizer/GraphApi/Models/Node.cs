﻿namespace FaceWebAuth.FacebookDataSynchronizer.GraphApi.Models
{
    /// <summary>
    /// Facebook node type
    /// </summary>
    public enum FacebookNodeType
    {
        unknown = 0,
        user,
        experience,
        photo,
        album,
        canvas,
        link,
        work,
        education,
        game,
        location
    }

    /// <summary>
    /// Node enumeration
    /// </summary>
    public enum Node
    {
        me,
    }
}
