﻿using VDS.RDF;

namespace FaceWebAuth.FacebookDataSynchronizer.GraphApi.Models
{
    /// <summary>
    /// Facebook graph interface
    /// </summary>
    public interface IFacebookGraph
    {
        /// <summary>
        /// Base graph
        /// </summary>
        IGraph BaseGraph { get; }
    }
}
