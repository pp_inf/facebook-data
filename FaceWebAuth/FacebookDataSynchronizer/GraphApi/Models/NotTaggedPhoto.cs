﻿using VDS.RDF;

namespace FaceWebAuth.FacebookDataSynchronizer.GraphApi.Models
{
    /// <summary>
    /// Not tagged photo
    /// </summary>
    public class NotTaggedPhoto
    {
        /// <summary>
        /// Photo subject
        /// </summary>
        public INode PhotoSubject { get; set; }
        /// <summary>
        /// Photo url
        /// </summary>
        public INode PhotoUrl { get; set; }
    }
}
