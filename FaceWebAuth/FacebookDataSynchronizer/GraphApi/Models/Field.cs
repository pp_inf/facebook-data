﻿namespace FaceWebAuth.FacebookDataSynchronizer.GraphApi.Models
{
    /// <summary>
    /// Field types enumeration
    /// </summary>
    public enum Field
    {
        id,
        name,
        birthday,
        gender,
        email,
        first_name,
        last_name,
        relationship_status,
        profile_type,
        created_time,
        link,
        type,
        start_date,
        about,
        category,
        source,
        hometown,
        url,
        images,
        picture,
        width,
        height
    }
}
