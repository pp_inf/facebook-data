﻿namespace FaceWebAuth.FacebookDataSynchronizer.GraphApi.Models
{
    /// <summary>
    /// Facebook edges enumeration
    /// </summary>
    public enum Edges
    {
        none = 0,
        education,
        school,
        albums,
        likes,
        work,
        employer,
        location,
        position,
        music,
        cover,
        movies,
        friends,
        games,
        place,
        hometown,
        photos,
        permissions
    }
}
