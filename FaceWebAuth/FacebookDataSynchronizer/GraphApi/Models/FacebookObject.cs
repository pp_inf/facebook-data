﻿namespace FaceWebAuth.FacebookDataSynchronizer.GraphApi.Models
{
    /// <summary>
    /// Test Facebook object
    /// </summary>
    public class FacebookObject
    {
        /// <summary>
        /// Facebook id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
    }

    /// <summary>
    /// Facebook graph api permissions enumeration
    /// </summary>
    public enum PermissionType
    {
        user_birthday,
        user_religion_politics,
        user_relationships,
        user_relationship_details,
        user_hometown,
        user_location,
        user_likes,
        user_education_history,
        user_work_history,
        user_website,
        user_events,
        user_photos,
        user_videos,
        user_friends,
        user_about_me,
        user_status,
        user_games_activity,
        user_tagged_places,
        user_posts,
        rsvp_event,
        email,
        read_insights,
        publish_actions,
        read_audience_network_insights,
        read_custom_friendlists,
        user_managed_groups,
        manage_pages,
        pages_manage_cta,
        pages_manage_instant_articles,
        pages_show_list,
        publish_pages,
        read_page_mailboxes,
        ads_management,
        ads_read,
        business_management,
        pages_messaging,
        pages_messaging_phone_number,
        pages_messaging_subscriptions,
        pages_messaging_payments,
        public_profile
    }

    /// <summary>
    /// Status enumaraion
    /// </summary>
    public enum Status
    {
        granted,
        declined
    }

    /// <summary>
    /// Facebook graph api permission
    /// </summary>
    public class ApiPermission
    {
        /// <summary>
        /// Permission name
        /// </summary>
        public string Permission { get; set; }
        /// <summary>
        /// Permission status
        /// </summary>
        public Status Status { get; set; }
    }
}
