﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FaceWebAuth.FacebookDataSynchronizer.GraphApi.Models;
using Newtonsoft.Json.Linq;
using VDS.RDF;

namespace FaceWebAuth.FacebookDataSynchronizer.GraphApi
{
    /// <summary>
    /// Facebook Graph Builder
    /// </summary>
    public class FacebookGraph : IFacebookGraph
    {
        /// <summary>
        /// Facebook Graph Uri
        /// </summary>
        public static Uri FacebookGraphUri { get; } = new Uri("https://www.facebook.com/fuseki/");
        
        /// <summary>
        /// Base graph
        /// </summary>
        public IGraph BaseGraph { get; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public FacebookGraph()
        {
            BaseGraph = new Graph { BaseUri = FacebookGraphUri };
            BaseGraph.NamespaceMap.AddNamespace("fb", UriFactory.Create("https://www.facebook.com#"));
            BaseGraph.NamespaceMap.AddNamespace("fb-obj", UriFactory.Create("https://www.facebook.com/object#"));
            BaseGraph.NamespaceMap.AddNamespace("fb-edge", UriFactory.Create("https://www.facebook.com/edge#"));
        }

        /// <summary>
        /// Add dynamically triples based on facebookObject
        /// </summary>
        /// <param name="facebookNodeId">Parent facebook object Id</param>
        /// <param name="type">Facebook object type</param>
        /// <param name="facebookObject">Object which will be maped to triples</param>
        public void AddFacebookObject(string facebookNodeId, FacebookNodeType type, IEnumerable<KeyValuePair<string, JToken>> facebookObject)
        {
            if (type != FacebookNodeType.unknown)
            {
                AddFacebookTriple(facebookNodeId, type);
            }
            foreach(KeyValuePair<string, JToken> fieldPair in facebookObject)
            {
                if (Enum.TryParse(fieldPair.Key, out Field fieldType))
                {
                    AddField(facebookNodeId, fieldType, fieldPair);
                }
                else
                {
                    if(Enum.TryParse(fieldPair.Key, out Edges edgeType))
                    {
                        AddEdge(facebookNodeId, edgeType, fieldPair.Value);
                    }
                }
            }
        }

        /// <summary>
        /// Add dynamically triples based on facebookObject
        /// </summary>
        /// <param name="facebookNodeId">Parent facebook object Id</param>
        /// <param name="type">Facebook object type</param>
        /// <param name="facebookObject">Object which will be maped to triples</param>
        public async Task AddFacebookObjectAsync(string facebookNodeId, FacebookNodeType type, IEnumerable<KeyValuePair<string, JToken>> facebookObject)
        {
            await Task.Run(()=> AddFacebookObject(facebookNodeId, type, facebookObject));
        }

        /// <summary>
        /// Add edge between facebook objects
        /// </summary>
        /// <param name="facebookParentNodeId">Parent id</param>
        /// <param name="edge">Edge enum</param>
        /// <param name="facebookObject">Json token with facebook data</param>
        private void AddEdge(string facebookParentNodeId, Edges edge, JToken facebookObject)
        {
            switch(edge)
            {
                case Edges.work :
                case Edges.education :
                    AddEdgeCollection(facebookParentNodeId, edge, facebookObject);
                    break;
                case Edges.location :
                    var elementId = facebookObject["latitude"]?.Value<string>() + facebookObject["longitude"]?.Value<string>();
                    AddFacebookTriple(facebookParentNodeId, edge, elementId);
                    AddFacebookObject(elementId, GetNodeType(edge), facebookObject.Value<JObject>());
                    break;
                default:
                    AddEdge(facebookParentNodeId, edge, facebookObject.Value<JObject>());
                    break;
            }
        }

        /// <summary>
        /// Add edge between facebook objects
        /// </summary>
        /// <param name="facebookParentNodeId">Parent id</param>
        /// <param name="edge">Edge enum</param>
        /// <param name="facebookObject">Json object with facebook data</param>
        private void AddEdge(string facebookParentNodeId, Edges edge, JObject facebookObject)
        {
            if (facebookObject.HasValues)
            {
                if (facebookObject.TryGetValue("data", out JToken dataValues))
                {
                    AddEdgeCollection(facebookParentNodeId, edge, dataValues);
                }
                else
                {
                    var elementId = facebookObject["id"]?.Value<string>();
                    AddFacebookTriple(facebookParentNodeId, edge, elementId);
                    AddFacebookObject(elementId, GetNodeType(edge), facebookObject);
                }
            }
        }

        /// <summary>
        /// Add edge between facebook object and collection of objects
        /// </summary>
        /// <param name="facebookParentNodeId">Parent id</param>
        /// <param name="edge">Edge enum</param>
        /// <param name="facebookObject">Json object with facebook data</param>
        private void AddEdgeCollection(string facebookParentNodeId, Edges edge, JToken facebookObject)
        {
            foreach (var fbData in facebookObject)
            {
                var fbId = fbData["id"]?.Value<string>();
                AddFacebookTriple(facebookParentNodeId, edge, fbId);
                AddFacebookObject(fbId, GetNodeType(edge), fbData.Value<JObject>());
            }
        }

        /// <summary>
        /// Adde field to facebook object
        /// </summary>
        /// <param name="facebookNodeId">Fcebook id</param>
        /// <param name="type">Field type enum</param>
        /// <param name="fieldPair">Field object</param>
        private void AddField(string facebookNodeId, Field type, KeyValuePair<string, JToken> fieldPair)
        {
            switch (type)
            {
                case Field.images:
                    AddImages(facebookNodeId, fieldPair.Value);
                    break;
                default:
                    AddFacebookTriple(facebookNodeId, type, fieldPair.Value);
                    break;
            }
        }

        /// <summary>
        /// Add images field to facebook object
        /// </summary>
        /// <param name="facebookParentNodeId">Facebook id</param>
        /// <param name="imagesCollection">Collection of images</param>
        private void AddImages(string facebookParentNodeId, IEnumerable<JToken> imagesCollection)
        {
            foreach(JToken image in imagesCollection)
            {
                AddFacebookTriple(facebookParentNodeId, Field.images, image["source"].Value<string>());
            }
        }

        /// <summary>
        /// Add facebook triples to base graph
        /// </summary>
        /// <param name="facebookNodeId">Facebook id</param>
        /// <param name="type">Node type enum</param>
        private void AddFacebookTriple(string facebookNodeId, FacebookNodeType type)
        {
            BaseGraph.Assert(CreateTriple(facebookNodeId,
                BaseGraph.CreateUriNode("rdf:type"),
                BaseGraph.CreateLiteralNode(type.ToString())));
        }

        /// <summary>
        /// Add facebook triples to base graph
        /// </summary>
        /// <param name="facebookNodeId">Facebook id</param>
        /// <param name="field">Field type enum</param>
        /// <param name="fieldValue">Field value</param>
        private void AddFacebookTriple(string facebookNodeId, Field field, object fieldValue)
        {
            BaseGraph.Assert(CreateTriple(facebookNodeId,
                BaseGraph.CreateUriNode($"fb:{field.ToString()}"),
                BaseGraph.CreateLiteralNode(fieldValue.ToString())));
        }

        /// <summary>
        /// Add facebook triples to base graph
        /// </summary>
        /// <param name="facebookNodeId">Facebook id</param>
        /// <param name="edge">Edge type enum</param>
        /// <param name="toFacebookNodeId">Facebook id</param>
        private void AddFacebookTriple(string facebookNodeId, Edges edge, string toFacebookNodeId)
        {
            BaseGraph.Assert(CreateTriple(facebookNodeId,
                BaseGraph.CreateUriNode($"fb-edge:{edge.ToString()}"),
                BaseGraph.CreateUriNode($"fb-obj:{toFacebookNodeId}")));
        }

        /// <summary>
        /// Add facebook triples to base graph
        /// </summary>
        /// <param name="facebookNodeId">Facebook id</param>
        /// <param name="predicate">Predicate Node</param>
        /// <param name="nodeObject">Object Node</param>
        private void AddFacebookTriple(string facebookNodeId, INode predicate, INode nodeObject)
        {
            BaseGraph.Assert(CreateTriple(facebookNodeId, predicate, nodeObject));
        }

        /// <summary>
        /// Create triples based of base graph
        /// </summary>
        /// <param name="facebookNodeId">Facebook id</param>
        /// <param name="predicate">Predicate Node</param>
        /// <param name="nodeObject">Object Node</param>
        /// <returns>Triple</returns>
        private Triple CreateTriple(string facebookNodeId, INode predicate, INode nodeObject)
        {
            return new Triple(BaseGraph.CreateUriNode($"fb-obj:{facebookNodeId}"), predicate, nodeObject);
        }

        /// <summary>
        /// Ged node type based on edge enum
        /// </summary>
        /// <param name="edgeType">Edge type enum</param>
        /// <returns>Facebook node type</returns>
        private FacebookNodeType GetNodeType(Edges edgeType)
        {
            switch(edgeType)
            {
                case Edges.albums:
                    return FacebookNodeType.album;
                case Edges.photos:
                    return FacebookNodeType.photo;
                case Edges.work:
                    return FacebookNodeType.work;
                case Edges.education:
                    return FacebookNodeType.education;
                case Edges.games:
                    return FacebookNodeType.game;
                case Edges.location:
                    return FacebookNodeType.location;
                default:
                    return FacebookNodeType.unknown;
            }
        }
    }
}
