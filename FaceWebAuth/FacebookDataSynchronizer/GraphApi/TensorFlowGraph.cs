﻿using System;
using FaceWebAuth.TensorFlow.Models;
using VDS.RDF;

namespace FaceWebAuth.FacebookDataSynchronizer.GraphApi
{
    /// <summary>
    /// TensorFlow graph Builder
    /// </summary>
    public class TensorFlowGraph
    {
        /// <summary>
        /// TensorFlow Graph Uri
        /// </summary>
        public static Uri TensorFlowGraphUri { get; } = new Uri("https://www.facebook.com/fuseki/");

        /// <summary>
        /// Base graph
        /// </summary>
        public IGraph BaseGraph { get; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public TensorFlowGraph()
        {
            BaseGraph = new Graph { BaseUri = TensorFlowGraphUri };
            BaseGraph.NamespaceMap.AddNamespace("fb", UriFactory.Create("https://www.facebook.com#"));
            BaseGraph.NamespaceMap.AddNamespace("fb-obj", UriFactory.Create("https://www.facebook.com/object#"));
            BaseGraph.NamespaceMap.AddNamespace("fb-edge", UriFactory.Create("https://www.facebook.com/edge#"));
            BaseGraph.NamespaceMap.AddNamespace("tensor", UriFactory.Create("https://www.tensorflow.org#"));
            BaseGraph.NamespaceMap.AddNamespace("tensor-obj", UriFactory.Create("https://www.tensorflow.org/object#"));
            BaseGraph.NamespaceMap.AddNamespace("tensor-edge", UriFactory.Create("https://www.tensorflow.org/edge#"));
        }

        /// <summary>
        /// Add classify image result to graph
        /// </summary>
        /// <param name="subjectNode">Subject node</param>
        /// <param name="classify">Classification result</param>
        public void AddClassifyImageResult(INode subjectNode, ClassifyImageResult classify)
        {
            INode subjClassifyNode = BaseGraph.CreateUriNode($"tensor-obj:{classify.Id}");
            AddTriple(Tools.CopyNode(subjectNode,BaseGraph,true), BaseGraph.CreateUriNode("tensor-edge:image_classify"), subjClassifyNode);
            AddTriple(subjClassifyNode, BaseGraph.CreateUriNode("rdf:type"), BaseGraph.CreateLiteralNode("image classify"));
            foreach(var tag in classify.Tags)
            {
                AddTriple(subjClassifyNode, BaseGraph.CreateUriNode("tensor:tag"), BaseGraph.CreateLiteralNode(tag));
            }
            AddTriple(subjClassifyNode, BaseGraph.CreateUriNode("tensor:score"), BaseGraph.CreateLiteralNode(classify.Score.ToString()));
        }

        /// <summary>
        /// Add triple to graph
        /// </summary>
        /// <param name="subject">Subject node</param>
        /// <param name="predicate">Predicate node</param>
        /// <param name="nodeObject">Object node</param>
        public void AddTriple(INode subject, INode predicate, INode nodeObject)
        {
            BaseGraph.Assert(new Triple(subject, predicate, nodeObject));
        }
    }
}
