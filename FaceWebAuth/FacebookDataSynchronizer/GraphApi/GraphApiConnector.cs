﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FaceWebAuth.Extensions;
using FaceWebAuth.FacebookDataSynchronizer.GraphApi.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using RestSharp.Portable;
using RestSharp.Portable.HttpClient;

namespace FaceWebAuth.FacebookDataSynchronizer.GraphApi
{
    /// <summary>
    /// Connector to Facebook Graph api
    /// </summary>
    public class GraphApiConnector : IDisposable
    {
        /// <summary>
        /// Disposed variable
        /// </summary>
        protected bool _isDisposed = false;
        /// <summary>
        /// Rest client
        /// </summary>
        private readonly RestClient _restClient;
        /// <summary>
        /// Access token to facebook graph api
        /// </summary>
        private readonly Parameter _accessToken;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="configuration">Configuration</param>
        /// <param name="accessToken">Facebook Access token</param>
        public GraphApiConnector(IConfiguration configuration, string accessToken)
        {
            ValidToken(accessToken);
            _restClient = new RestClient(configuration.GetFacebookGrapApiUri());
            _accessToken = new Parameter { Name = "access_token", Value = accessToken };
        }

        /// <summary>
        /// Get complex data about access token owner
        /// </summary>
        /// <returns>Json Object representation</returns>
        protected async Task<JObject> GetComplexData()
        {
            var request = new RestRequest("me");
            request.AddParameter(_accessToken);
            request.AddParameter("fields", @"id,name,birthday,gender,albums{created_time,link,likes{id,link,name,picture{url},profile_type},photos{images,picture,name,link,likes{id,name}}},education,email,first_name,last_name,religion,work,books{id,about,name,category,cover},music{id,name,about,category,cover},movies{id,name,about,category},cover,relationship_status,hometown,languages,friends{id,name,first_name,last_name,hometown,gender,relationship_status},groups{id,name,cover,picture},games");
            var response = await _restClient.Execute(request);
            ValidResponse(response);
            var jObject = JObject.Parse(response.Content);
            ValidMessage(jObject);
            return jObject;
        }

        /// <summary>
        /// Get Facebook Data
        /// </summary>
        /// <param name="node">Node enum</param>
        /// <param name="fields">Fields parameters</param>
        /// <returns>Json Object representation</returns>
        protected async Task<JObject> Get(Node node, params Field[] fields)
        {
            return await Get(node.ToString(), Edges.none, fields);
        }

        /// <summary>
        /// Get Facebook Data
        /// </summary>
        /// <param name="node">Node enum</param>
        /// <param name="edge">Edge enum</param>
        /// <param name="fields">Fields parameters</param>
        /// <returns></returns>
        protected async Task<JObject> Get(Node node, Edges edge, params Field[] fields)
        {
            return await Get(node.ToString(), edge, fields);
        }

        /// <summary>
        /// Get Facebook Data
        /// </summary>
        /// <param name="node">Node string</param>
        /// <param name="edge">Edge enum</param>
        /// <param name="fields">Fields parameters</param>
        /// <returns></returns>
        protected async Task<JObject> Get(string node, Edges edge, params Field[] fields)
        {
            string resource = (edge == Edges.none) ? node : $"{node}/{edge}";
            var request = new RestRequest(resource);
            request.AddParameter(_accessToken);
            if (fields != null && fields.Any())
            {
                request.AddParameter("fields", string.Join(",", fields));
            }
            var response = await _restClient.Execute(request);
            ValidResponse(response);
            var jObject = JObject.Parse(response.Content);
            ValidMessage(jObject);
            return jObject;
            /*To Do - dodaj obsługę paginacji
             * "paging": {
             * "cursors": {
             * "before": "NDEyMTQ2MDE4ODIyNDU1",
             * "after": "NTMzODg3NjQ5OTgxNjI0"    }}*/
        }

        /// <summary>
        /// Validate if token is correct
        /// </summary>
        /// <param name="accessToken">Facebook access token</param>
        private void ValidToken(string accessToken)
        {
            if (string.IsNullOrEmpty(accessToken))
            {
                throw new ArgumentNullException("Access token can't be null or empty.");
            }
        }

        /// <summary>
        /// Validate REST response
        /// </summary>
        /// <param name="response">Rest object</param>
        private void ValidResponse(IRestResponse response)
        {
            if (!response.IsSuccess)
            {
                throw new Exception(response.StatusCode.ToString());
            }
        }

        /// <summary>
        /// validate message
        /// </summary>
        /// <param name="jObject">Json object</param>
        private void ValidMessage(JObject jObject)
        {
            if (jObject["error"] != null)
            {
                throw new Exception(jObject["error"].Value<string>("message"));
            }
        }

        /// <summary>
        /// Dispose imlementation
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose imlementation
        /// </summary>
        /// <param name="disposing"></param>
        virtual protected void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    // tu zwalniamy zasoby zarządzane (standardowe klasy)
                }
                // tu zwalniamy zasoby niezarządzane (np. strumienie, obiekty COM itp.)
                _restClient.Dispose();
            }
            _isDisposed = true;
        }

        /// <summary>
        /// Destructor
        /// </summary>
        ~GraphApiConnector()
        {
            Dispose(false);
        }
    }
}
