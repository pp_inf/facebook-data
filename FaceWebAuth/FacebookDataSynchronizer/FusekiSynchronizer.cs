using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FaceWebAuth.FacebookDataSynchronizer.GraphApi;
using FaceWebAuth.FacebookDataSynchronizer.GraphApi.Models;
using FaceWebAuth.TensorFlow;
using Microsoft.Extensions.Configuration;

namespace FaceWebAuth.FacebookDataSynchronizer
{
    /// <summary>
    /// Synchronizer with fuseki connection
    /// </summary>
    public class FusekiSynchronizer : FusekiApiConnector
    {
        /// <summary>
        /// Facebook collector object
        /// </summary>
        private readonly FacebookCollector _faceCollector;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="configuration">Configuration</param>
        public FusekiSynchronizer(IConfiguration configuration) : base(configuration)
        {
        }
        /// <summary>
        /// Constructor with facebook collector
        /// </summary>
        /// <param name="configuration">Configuration</param>
        /// <param name="accessToken">Access token to Facebook Graph Api</param>
        public FusekiSynchronizer(IConfiguration configuration, string accessToken) : base(configuration)
        {
            _faceCollector = new FacebookCollector(configuration, accessToken);
        }

        /// <summary>
        /// Synchronize facebook user data with fuseki
        /// </summary>
        /// <returns>Task</returns>
        public async Task SyncFacebookMeDataDynamic()
        {
            var fbGraph = await _faceCollector.MeDynamicComplex();
            var insertTask = PerformUpdateGraph(fbGraph);
            await insertTask;
        }

        /// <summary>
        /// Classify unclassified images
        /// </summary>
        /// <returns>Classified uris</returns>
        public async Task<IEnumerable<string>> ClassifyFacebookImages()
        {
            var classifiedUris = new List<string>();
            var tensor = new TensorMachineLearning(_configuration);
            var tensorGraph = new TensorFlowGraph();
            foreach (var notTaggedPhotos in (await GetNotTaggedPhotos()))
            {
                if (notTaggedPhotos?.PhotoSubject != null && notTaggedPhotos?.PhotoUrl != null)
                {
                    try
                    {
                        var classifications = await tensor.ClassifyImageByUri(notTaggedPhotos.PhotoUrl.ToString());
                        foreach (var cl in classifications)
                        {
                            tensorGraph.AddClassifyImageResult(notTaggedPhotos.PhotoSubject, cl);
                        }
                        classifiedUris.Add(notTaggedPhotos.PhotoUrl.ToString());
                    }
                    catch { };
                }
            }
            if (!tensorGraph.BaseGraph.IsEmpty)
            {
                var insertTask = PerformUpdateGraph(tensorGraph.BaseGraph);
                await insertTask;
            }
            return classifiedUris;
        }

        /// <summary>
        /// Get unclassified photos from RDF storage
        /// </summary>
        /// <returns>Collection with untagged photos details</returns>
        private async Task<IEnumerable<NotTaggedPhoto>> GetNotTaggedPhotos()
        {
            var result = await PerformSelectQuery(
                @"PREFIX fb: <https://www.facebook.com/#>
PREFIX fb-obj: <https://www.facebook.com/object#>
PREFIX fb-edge: <https://www.facebook.com/edge#>
PREFIX tensor-obj: <https://www.tensorflow.org/object#>
PREFIX tensor-edge: <https://www.tensorflow.org/edge#>

SELECT DISTINCT ?s (SAMPLE(?img) AS ?image)
FROM <https://www.facebook.com/fuseki/>
WHERE 
{
    ?s fb:images|fb:source ?img .
    FILTER NOT EXISTS {?s tensor-edge:image_classify ?classify}
} GROUP BY ?s");
            return result.Select(f => new NotTaggedPhoto { PhotoSubject = f.Value("s"), PhotoUrl = f.Value("image") }); ;
        }

        /// <summary>
        /// Dispose implementation
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                _faceCollector?.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
