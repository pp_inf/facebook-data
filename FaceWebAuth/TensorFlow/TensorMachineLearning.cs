﻿using FaceWebAuth.Extensions;
using FaceWebAuth.TensorFlow.Models;
using Microsoft.Extensions.Configuration;
using RestSharp.Portable;
using RestSharp.Portable.HttpClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using NLog;

namespace FaceWebAuth.TensorFlow
{
    /// <summary>
    /// Class with TensorFlow machine learning implementations
    /// </summary>
    public class TensorMachineLearning
    {
        /// <summary>
        /// Logger object
        /// </summary>
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// Configuration
        /// </summary>
        private IConfiguration _configuration;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="configuration"></param>
        public TensorMachineLearning(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Classify image
        /// </summary>
        /// <param name="imagePath">Path to image file</param>
        /// <returns>Image classification results</returns>
        public async Task<IEnumerable<ClassifyImageResult>> ClassifyImage(string imagePath)
        {
            string modelDir = @"./TensorFlow/PythonData/";

            string command = $"./TensorFlow/PythonData/classify_image.py --image_file \"{imagePath}\" --model_dir {modelDir}";
            var handler = new ClassifyImageResultHandler();
            await Task.Run(()=>
            {
                var process = new Process()
                {
                    StartInfo = new ProcessStartInfo(_configuration.GetPythonAplication())
                    {
                        UseShellExecute = false,
                        RedirectStandardInput = false,
                        RedirectStandardOutput = true,
                        Arguments = command,
                        CreateNoWindow = true
                    }
                };
                process.OutputDataReceived += new DataReceivedEventHandler(handler.AddResult);
                process.Start();
                process.BeginOutputReadLine();
                process.WaitForExit();
            });
            return handler.Result;
        }

        /// <summary>
        /// Classify imgage by Uri
        /// </summary>
        /// <param name="uri">Uri to image</param>
        /// <returns>Image classification results</returns>
        public async Task<IEnumerable<ClassifyImageResult>> ClassifyImageByUri(string uri)
        {
            var imagePath = await DownloadImageToTemp((!string.IsNullOrEmpty(uri)) ? uri : throw new Exception("Missing uri"));
            var result = await ClassifyImage(imagePath);
            File.Delete(imagePath);
            _logger.Info($"Classified: {uri}");
            return result;
        }

        /// <summary>
        /// Download image to temporary path
        /// </summary>
        /// <param name="url">Url to image</param>
        /// <returns>Path to image file</returns>
        private async Task<string> DownloadImageToTemp(string url)
        {
            string tempFilePath;
            using (var client = new RestClient(url))
            {
                var request = new RestRequest();
                var result = await client.Execute(request);
                if (result.IsSuccess)
                {
                    tempFilePath = Path.Combine(_configuration.GetTempFolder(), Guid.NewGuid().ToString());
                    await File.WriteAllBytesAsync(tempFilePath, result.RawBytes);
                }
                else
                {
                    throw new Exception("Wrong result");
                }
            }
            return tempFilePath;
        }
    }
}
