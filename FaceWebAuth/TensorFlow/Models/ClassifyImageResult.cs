﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace FaceWebAuth.TensorFlow.Models
{
    /// <summary>
    /// Image classification handler
    /// </summary>
    internal class ClassifyImageResultHandler
    {
        public IList<ClassifyImageResult> Result { get; } = new List<ClassifyImageResult>();

        public void AddResult(object sendingProcess, DataReceivedEventArgs outLine)
        {
            if(!string.IsNullOrEmpty(outLine.Data))
            {
                Result.Add(new ClassifyImageResult(outLine.Data));
            }
        }
    }

    /// <summary>
    /// Image classification result
    /// </summary>
    public class ClassifyImageResult
    {
        /// <summary>
        /// MD5 Implementation
        /// </summary>
        private static readonly MD5 _hash = MD5.Create();
        /// <summary>
        /// Constructor
        /// </summary>
        public ClassifyImageResult() { }
        /// <summary>
        /// Constructor which base on classify string
        /// </summary>
        /// <param name="classifyString">String with classification from python script output</param>
        public ClassifyImageResult(string classifyString)
        {
            if (classifyString.Contains("score"))
            {
                Id = GetMd5Hash(classifyString);
                var clStr = classifyString.TrimEnd(')');
                var data = clStr.Split("(score =");
                Tags = data[0].Split(',').Select(f => f.Trim());
                Score = float.Parse(data[1], CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// Classification identifier
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Classification tags
        /// </summary>
        public IEnumerable<string> Tags { get; set; }
        /// <summary>
        /// Score in percent with prediction
        /// </summary>
        public float Score { get; set; }

        /// <summary>
        /// Get MD5 hash based on string
        /// </summary>
        /// <param name="input">Inptut string</param>
        /// <returns>MD5 hash</returns>
        static string GetMd5Hash(string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = _hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
    }
}
