﻿using Microsoft.Extensions.Configuration;

namespace FaceWebAuth.Extensions
{
    /// <summary>
    /// Configuration extension
    /// </summary>
    public static class ConfigurationExtensions
    {
        /// <summary>
        /// Get facebook graph api uri
        /// </summary>
        /// <param name="configuration">Configuration</param>
        /// <returns>Facebook graph api uri</returns>
        public static string GetFacebookGrapApiUri(this IConfiguration configuration)
        {
            return configuration?.GetSection("Facebook")?["GrapApiUri"];
        }

        /// <summary>
        /// Get Facebook app id
        /// </summary>
        /// <param name="configuration">Configuration</param>
        /// <returns>Facebook app id</returns>
        public static string GetFacebookAppId(this IConfiguration configuration)
        {
            return configuration?.GetSection("Facebook")?["AppId"];
        }

        /// <summary>
        /// Get Facebook app secret
        /// </summary>
        /// <param name="configuration">Configuration</param>
        /// <returns>Facebook app secret</returns>
        public static string GetFacebookAppSecret(this IConfiguration configuration)
        {
            return configuration?.GetSection("Facebook")?["AppSecret"];
        }

        /// <summary>
        /// Get fuseki server uri
        /// </summary>
        /// <param name="configuration">Configuration</param>
        /// <returns>Fuseki server uri</returns>
        public static string GetFusekiServiceUri(this IConfiguration configuration)
        {
            return configuration?.GetSection("Fuseki")?["ServiceUri"];
        }

        /// <summary>
        /// Get temporary folder
        /// </summary>
        /// <param name="configuration">Configuration</param>
        /// <returns>Temporary folder</returns>
        public static string GetTempFolder(this IConfiguration configuration)
        {
            return configuration?.GetValue<string>("TEMP");
        }

        /// <summary>
        /// Get Python application name
        /// </summary>
        /// <param name="configuration">Configuration</param>
        /// <returns>Python name</returns>
        public static string GetPythonAplication(this IConfiguration configuration)
        {
            return configuration?.GetValue<string>("PYTHON") ?? "python";
        }
    }
}
