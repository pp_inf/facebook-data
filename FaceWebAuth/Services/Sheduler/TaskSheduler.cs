﻿using FaceWebAuth.Services.Sheduler.Jobs;
using Microsoft.Extensions.Configuration;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FaceWebAuth.Services.Sheduler
{
    /// <summary>
    /// Task scheduler
    /// </summary>
    public class TaskSheduler
    {
        /// <summary>
        /// Scheduler instance
        /// </summary>
        public static TaskSheduler Instance { get; } = new TaskSheduler();
        
        /// <summary>
        /// Default scheduler
        /// </summary>
        private IScheduler _defaultScheduler;
        /// <summary>
        /// Configuration
        /// </summary>
        private IConfiguration _configuration;
        /// <summary>
        /// Initialization flag
        /// </summary>
        private bool _initialized;

        /// <summary>
        /// Constructor
        /// </summary>
        private TaskSheduler()
        {
            _defaultScheduler = StdSchedulerFactory.GetDefaultScheduler().Result;
        }

        /// <summary>
        /// Scheduler initialization request
        /// </summary>
        /// <param name="configuration">Configuration</param>
        /// <returns>Task</returns>
        public static async Task Initialize(IConfiguration configuration)
        {
            if(Instance._initialized)
            {
                throw new Exception("TaskSheduler cannot be initilized twice");
            }
            Instance._initialized = true;
            Instance._configuration = configuration;
            await Instance._defaultScheduler.Start();
            await Instance.InitFbImageClassifyJob();
        }

        /// <summary>
        /// Initialization facebook image job
        /// </summary>
        /// <returns>Task</returns>
        private async Task InitFbImageClassifyJob()
        {
            var jb = new JobDataMap(new Dictionary<string, IConfiguration> { { "configuration", _configuration } });
            
            // define the job and tie it to our HelloJob class
            IJobDetail job = JobBuilder.Create<FbImageClassifyJob>()
                .WithIdentity("FbImageClassifyJob", "group1")
                .SetJobData(jb)
                .Build();

            // Trigger the job to run now, and then repeat every 20 minutes
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("trigger1", "group1")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInMinutes(20)
                    .RepeatForever())
                .Build();

            // Tell quartz to schedule the job using our trigger
            await _defaultScheduler.ScheduleJob(job, trigger);
        }

    }
}
