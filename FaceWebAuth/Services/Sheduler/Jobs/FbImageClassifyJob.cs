﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FaceWebAuth.FacebookDataSynchronizer;
using Microsoft.Extensions.Configuration;
using NLog;
using Quartz;

namespace FaceWebAuth.Services.Sheduler.Jobs
{
    /// <summary>
    /// Facebook images classification job
    /// </summary>
    public class FbImageClassifyJob : IJob
    {
        /// <summary>
        /// Logger object
        /// </summary>
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Execute method used by scheduler
        /// </summary>
        /// <param name="context">Job context</param>
        /// <returns>Task</returns>
        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                var config = (IConfiguration)context.JobDetail.JobDataMap.Get("configuration");
                IEnumerable<string> result;
                using (var fuseki = new FusekiSynchronizer(config))
                {
                    result = await fuseki.ClassifyFacebookImages();
                }
            }
            catch(Exception exception)
            {
                _logger.Error(exception, exception.Message);
            }
        }
    }
}
