# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Default docker commands to build and run container ###

Go to release folder
docker build -t faceweb:latest .
docker build -t ms/tensorflow:latest .
docker run -itd --name FaceWeb -p 80:80 -e "ASPNETCORE_ENVIRONMENT=Development" -v /var/log/FaceWeb:/var/log/FaceWeb faceweb:latest

### TensorFlow ###

To use tensorflow install python in version 3.n
and install packages:
pip3 install --upgrade tensorflow
add python folder to Environment variable
example path: c:\Users\UserName\AppData\Local\Programs\Python\Python36